package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func Req() []byte {
	html, err := http.Get("https://api.coindesk.com/v1/bpi/currentprice/CZK.json")
	if err != nil {
		panic(err)
	}
	defer html.Body.Close()
	body, err := ioutil.ReadAll(html.Body)
	if err != nil {
		panic(err)
	}
	return body
}

func Cut() {
	// struct generated by https://mholt.github.io/json-to-go/
	type inJ struct {
		Time struct {
			Updated    string    `json:"updated"`
			UpdatedISO time.Time `json:"-"`
			Updateduk  string    `json:"-"`
		} `json:"time"`
		Disclaimer string `json:"disclaimer"`
		Bpi        struct {
			USD struct{} `json:"-"`
			CZK struct {
				Code        string  `json:"code"`
				Rate        string  `json:"rate"`
				Description string  `json:"-"`
				RateFloat   float64 `json:"rate_float"`
			} `json:"CZK"`
		} `json:"bpi"`
	}
	var j inJ
	ouJ := Req()
	err := json.Unmarshal(ouJ, &j)
	if err != nil {
		panic(err)
	}
	// j.Bpi.CZK.RateFloat * amount of bitcoins
	fmt.Printf("\n BITCOIN \n %v:\n %v: %v\n %v \n\n %v\n", j.Time.Updated, j.Bpi.CZK.Code, j.Bpi.CZK.Rate, j.Bpi.CZK.RateFloat*0.0089, j.Disclaimer)
}

func main() {
	Cut()
}
